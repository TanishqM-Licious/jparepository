package com.example.tanishq.repository;

import com.example.tanishq.model.address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface addressRepository extends JpaRepository<address, Integer> {

}
