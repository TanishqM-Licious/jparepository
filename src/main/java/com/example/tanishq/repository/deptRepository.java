package com.example.tanishq.repository;

import com.example.tanishq.model.dept;
import org.springframework.data.jpa.repository.JpaRepository;

public interface deptRepository extends JpaRepository<dept, Integer>{

}
