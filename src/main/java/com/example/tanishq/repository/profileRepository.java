package com.example.tanishq.repository;

import com.example.tanishq.model.profile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface profileRepository extends JpaRepository<profile, Integer>{
}
