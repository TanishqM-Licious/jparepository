package com.example.tanishq.resource;


import com.example.tanishq.model.dept;
import com.example.tanishq.repository.deptRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/rest/dept")
public class deptResource {

    @Autowired
    deptRepository deptRepository;

    @GetMapping(value = "/all")
    public List<dept> getAll() {
        return deptRepository.findAll();
    }

    @PostMapping(value = "/load")
    public List<dept> persist(@RequestBody final dept users){
        deptRepository.save(users);
        return deptRepository.findAll();
    }

    @PutMapping(value = "/{id}")
    public dept update(@RequestBody final dept user,@PathVariable final int id){
        Optional<dept> d = deptRepository.findById(id);
        dept user1 = d.get();
        user1.setName(user.getName());
        deptRepository.save(user1);
        return user1;
    }

    @DeleteMapping(value = "/{id}")
    public dept delete(@PathVariable final int id){
        Optional<dept> d =deptRepository.findById(id);
        dept user1 = d.get();
        deptRepository.delete(user1);
        return user1;
    }

}
