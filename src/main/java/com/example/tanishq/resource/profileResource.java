package com.example.tanishq.resource;

import com.example.tanishq.model.profile;
import com.example.tanishq.repository.profileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/rest/profile")
public class profileResource {

    @Autowired
    profileRepository profileRepository;

    @GetMapping(value = "/all")
    public List<profile> getAll() {
        return profileRepository.findAll();
    }

    @PostMapping(value = "/load")
    public List<profile> persist(@RequestBody final profile users){
        profileRepository.save(users);
        return profileRepository.findAll();
    }

    @PutMapping(value = "/{id}")
    public profile update(@RequestBody final profile user,@PathVariable final int id){
        Optional<profile> d = profileRepository.findById(id);
        profile user1 = d.get();
        user1.setName(user.getName());
        profileRepository.save(user1);
        return user1;
    }

    @DeleteMapping(value = "/{id}")
    public profile delete(@PathVariable final int id){
        Optional<profile> d = profileRepository.findById(id);
        profile user1 = d.get();
        profileRepository.delete(user1);
        return user1;
    }

}
