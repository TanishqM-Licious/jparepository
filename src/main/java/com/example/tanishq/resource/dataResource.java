package com.example.tanishq.resource;


import com.example.tanishq.model.data;
import com.example.tanishq.repository.dataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/rest/users")
public class dataResource {

    @Autowired
    dataRepository dataRepository;

   @GetMapping(value = "/all")
   public List<data> getAll() {
       return dataRepository.findAll();
   }

   @PostMapping(value = "/load")
    public List<data> persist(@RequestBody final data users){
       dataRepository.save(users);
       return dataRepository.findAll();
   }

   @PutMapping(value = "/{id}")
    public data update(@RequestBody final data user,@PathVariable final int id){
       Optional<data> d = dataRepository.findById(id);
       data user1 = d.get();
       user1.setGender(user.getGender());
       user1.setName(user.getName());
       dataRepository.save(user1);
       return user1;
   }

   @DeleteMapping(value = "/{id}")
    public data delete(@PathVariable final int id){
       Optional<data> d = dataRepository.findById(id);
       data user1 = d.get();
       dataRepository.delete(user1);
       return user1;
   }



}
