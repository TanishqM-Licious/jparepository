package com.example.tanishq.resource;

import com.example.tanishq.model.address;
import com.example.tanishq.repository.addressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(value = "/rest/address")
public class addressResource {


    @Autowired
    addressRepository addressRepository;

    @GetMapping(value = "/all")
    public List<address> getAll() {
        return addressRepository.findAll();
    }

    @PostMapping(value = "/load")
    public List<address> persist(@RequestBody final address users){
        addressRepository.save(users);
        return addressRepository.findAll();
    }

    @PutMapping(value = "/{id}")
    public address update(@RequestBody final address user,@PathVariable final int id){
        Optional<address> d = addressRepository.findById(id);
        address user1 = d.get();
        user1.setCity(user.getCity());
        addressRepository.save(user1);
        return user1;
    }

    @DeleteMapping(value = "/{id}")
    public address delete(@PathVariable final int id){
        Optional<address> d = addressRepository.findById(id);
        address user1 = d.get();
        addressRepository.delete(user1);
        return user1;
    }

}
