package com.example.tanishq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = "com.example.tanishq.repository")
@SpringBootApplication
public class TanishqApplication {

	public static void main(String[] args) {
		SpringApplication.run(TanishqApplication.class, args);
	}

}
