package com.example.tanishq.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class dept {

    @Id
    @Column(name = "Dept_ID")
    private Integer id;
    @Column(name = "Name")
    private java.lang.String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
